# EFREI-Network-2-2023

Ici vous trouverez tous les supports de cours concernant le cours Virtualisation Réseau EFREI de l'année 2023-2024.

## [Cours](./cours/README.md)

- [Adresses IP](./cours/ip/README.md)
- [Routing](./cours/routing/README.md)
- [NAT](./cours/nat/README..md)

## [TP](./tp/README.md)

- [TP1 : Remise dans le bain](./tp/1/README.md)

## [Memo](./memo/README.md)

- [Installation de VM Rocky Linux](./memo/install_vm.md)
- [Manipulations Réseau sous Rocky Linux](./memo/rocky_network.md)
